/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.8243451463790447, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [1.0, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.9821428571428571, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [1.0, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [1.0, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [1.0, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.41379310344827586, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [1.0, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.9827586206896551, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [1.0, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [1.0, 500, 1500, "getAllEvents"], "isController": false}, {"data": [1.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.9827586206896551, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.9655172413793104, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.9827586206896551, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [1.0, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [1.0, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.9821428571428571, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [1.0, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.9827586206896551, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.9310344827586207, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [1.0, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [1.0, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [1.0, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [1.0, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.9655172413793104, 500, 1500, "portfolioByID"], "isController": false}, {"data": [1.0, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [1.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.5, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [1.0, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [1.0, 500, 1500, "getPastEvents"], "isController": false}, {"data": [1.0, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [1.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.5, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.48333333333333334, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.9827586206896551, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [1.0, 500, 1500, "getAllArea"], "isController": false}, {"data": [1.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1298, 0, 0.0, 656.3713405238824, 3, 11780, 21.0, 2174.1000000000004, 3406.5499999999993, 10750.16, 4.208219320200749, 205.39296448970475, 7.496314920617681], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["findAllChildActiveSubsidies", 28, 0, 0.0, 21.67857142857143, 14, 123, 17.0, 25.30000000000002, 84.29999999999976, 123.0, 0.09879191599864513, 0.1106344039760218, 0.1610192459001355], "isController": false}, {"data": ["findAllFeeGroup", 28, 0, 0.0, 51.535714285714285, 7, 964, 9.0, 37.800000000000296, 631.4499999999979, 964.0, 0.10328254045540222, 0.09481014455867001, 0.11125062707256705], "isController": false}, {"data": ["getUpcomingEvents", 29, 0, 0.0, 4.8965517241379315, 3, 8, 5.0, 6.0, 7.5, 8.0, 0.10798613309104721, 0.06295675923374529, 0.1550191559021869], "isController": false}, {"data": ["getListDomainByLevel", 29, 0, 0.0, 15.103448275862071, 12, 34, 14.0, 19.0, 28.0, 34.0, 0.09960398828107558, 0.52264254640172, 0.17712779556624866], "isController": false}, {"data": ["getPendingEvents", 30, 0, 0.0, 13.833333333333334, 11, 23, 13.0, 21.400000000000013, 23.0, 23.0, 0.11236291724096602, 0.05607172920911488, 0.14615957594235032], "isController": false}, {"data": ["findAllCurrentFeeTier", 29, 0, 0.0, 931.1724137931034, 728, 1676, 785.0, 1591.0, 1654.5, 1676.0, 0.1021036880556289, 0.7510961293019981, 0.2584499603908107], "isController": false}, {"data": ["getPortfolioTermByYear", 59, 0, 0.0, 8.389830508474578, 4, 24, 7.0, 14.0, 18.0, 24.0, 0.20105845348563794, 0.20096527226381594, 0.2551667922572049], "isController": false}, {"data": ["findAllClassResources", 29, 0, 0.0, 22.93103448275862, 3, 519, 5.0, 10.0, 266.5, 519.0, 0.10321900938225204, 0.060580688123763146, 0.1433373352944945], "isController": false}, {"data": ["listBulkInvoiceRequest", 28, 0, 0.0, 30.607142857142854, 25, 82, 28.0, 32.800000000000026, 67.1499999999999, 82.0, 0.10030413647094225, 0.14951515376086777, 0.15192550358049944], "isController": false}, {"data": ["getAllEvents", 28, 0, 0.0, 27.142857142857146, 23, 37, 25.5, 36.1, 37.0, 37.0, 0.10856668708754352, 0.6978378265724722, 0.19380849999612262], "isController": false}, {"data": ["findAllCustomSubsidies", 30, 0, 0.0, 21.266666666666662, 17, 42, 19.5, 28.60000000000001, 38.14999999999999, 42.0, 0.10746640779203101, 0.1287497940227184, 0.13433300974003876], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 29, 0, 0.0, 243.17241379310343, 174, 861, 192.0, 408.0, 678.5, 861.0, 0.10304918999783241, 8.587995818734699, 0.14843511254570588], "isController": false}, {"data": ["getAllChildDiscounts", 29, 0, 0.0, 70.62068965517241, 12, 1060, 14.0, 28.0, 822.5, 1060.0, 0.09835042595908622, 0.06012438149451951, 0.16202848495408054], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 29, 0, 0.0, 49.27586206896551, 19, 718, 22.0, 44.0, 391.5, 718.0, 0.1054672015187277, 0.07590754640556867, 0.13832270667934699], "isController": false}, {"data": ["getMyDownloadPortfolio", 28, 0, 0.0, 8.464285714285715, 6, 18, 8.0, 12.100000000000001, 15.749999999999986, 18.0, 0.09949612320462799, 0.05849283805584575, 0.10211955614068752], "isController": false}, {"data": ["getListDomain", 58, 0, 0.0, 11.879310344827589, 4, 33, 11.5, 20.300000000000004, 24.0, 33.0, 0.20204624072067803, 0.8459189489763571, 0.3347377415845999], "isController": false}, {"data": ["getAdvancePaymentReceipts", 28, 0, 0.0, 35.17857142857144, 13, 580, 15.0, 17.0, 326.6499999999984, 580.0, 0.10431374594387174, 0.15687809448590087, 0.16635190149056892], "isController": false}, {"data": ["getLessonPlan", 29, 0, 0.0, 5.517241379310344, 4, 21, 5.0, 6.0, 16.0, 21.0, 0.10227292765072031, 0.06661722924124068, 0.16399623750242456], "isController": false}, {"data": ["findAllProgramBillingUpload", 29, 0, 0.0, 56.275862068965516, 23, 913, 24.0, 42.0, 478.0, 913.0, 0.11002393969170533, 0.14855603063977024, 0.16406890225511137], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 29, 0, 0.0, 199.17241379310343, 60, 1127, 74.0, 1083.0, 1116.0, 1127.0, 0.1005798951194473, 0.14263947006534225, 0.18082772159658447], "isController": false}, {"data": ["getChildSemesterEvaluation", 29, 0, 0.0, 10.379310344827585, 8, 28, 9.0, 14.0, 22.5, 28.0, 0.1048176353864337, 0.08076280695302362, 0.16070672612959075], "isController": false}, {"data": ["getCentreManagementConfig", 28, 0, 0.0, 8.67857142857143, 7, 12, 9.0, 10.100000000000001, 11.549999999999997, 12.0, 0.09681613233382202, 0.11421278111255566, 0.15675893301706728], "isController": false}, {"data": ["updateClassResourceOrder", 28, 0, 0.0, 6.714285714285714, 5, 16, 6.0, 7.500000000000007, 14.199999999999989, 16.0, 0.10200029871516052, 0.06016423869527046, 0.09701981537945932], "isController": false}, {"data": ["getChildPortfolio", 30, 0, 0.0, 3505.0333333333338, 3229, 4553, 3403.5, 4153.6, 4348.4, 4553.0, 0.11238564760356337, 0.2100462876679229, 0.2718547354629165], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 28, 0, 0.0, 5.428571428571429, 4, 20, 5.0, 6.100000000000001, 14.149999999999963, 20.0, 0.098378165668831, 0.05889239800292324, 0.17955936683109874], "isController": false}, {"data": ["findAllCentreForSchool", 56, 0, 0.0, 2223.303571428571, 1870, 3371, 2060.0, 3103.0, 3192.75, 3371.0, 0.20251406749504566, 184.47637993221204, 0.4979789276880126], "isController": false}, {"data": ["portfolioByID", 29, 0, 0.0, 65.0344827586207, 23, 657, 25.0, 39.0, 586.0, 657.0, 0.10510749960131638, 0.16700185727670092, 0.3694159092432985], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 30, 0, 0.0, 20.433333333333337, 16, 37, 19.0, 29.0, 33.699999999999996, 37.0, 0.10416341156005542, 0.06520385430663625, 0.13864719722300345], "isController": false}, {"data": ["invoicesByFkChild", 29, 0, 0.0, 39.724137931034484, 25, 228, 31.0, 49.0, 147.0, 228.0, 0.10300819447947118, 0.29721346738902643, 0.2993675652059631], "isController": false}, {"data": ["findAllFeeDraft", 29, 0, 0.0, 995.1724137931035, 937, 1119, 989.0, 1073.0, 1097.5, 1119.0, 0.09791541458737094, 0.055651143837744034, 0.15595707147656446], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 29, 0, 0.0, 21.965517241379313, 4, 471, 5.0, 17.0, 248.0, 471.0, 0.10111329221392784, 0.06102345174629629, 0.1104939199095559], "isController": false}, {"data": ["findAllConsolidatedRefund", 30, 0, 0.0, 2285.3, 2119, 3065, 2202.0, 2627.2, 2992.95, 3065.0, 0.10296222316032248, 0.14669770655938003, 0.17787126247129925], "isController": false}, {"data": ["getPastEvents", 29, 0, 0.0, 11.551724137931036, 8, 38, 10.0, 16.0, 31.5, 38.0, 0.10107100412299992, 0.05014069345164449, 0.12732577667838857], "isController": false}, {"data": ["getMyDownloadAlbum", 29, 0, 0.0, 22.86206896551725, 6, 321, 7.0, 21.0, 224.5, 321.0, 0.10187413275253368, 0.06665592670331794, 0.16086960220785834], "isController": false}, {"data": ["getRefundChildBalance", 28, 0, 0.0, 13.107142857142858, 9, 68, 11.0, 14.100000000000016, 48.199999999999875, 68.0, 0.10795014245563442, 0.10299539958901838, 0.16898835776990426], "isController": false}, {"data": ["findAllUploadedGiroFiles", 28, 0, 0.0, 609.25, 527, 1109, 568.5, 741.5000000000001, 990.1999999999992, 1109.0, 0.0961211675975544, 27.30246134555903, 0.13751710012736057], "isController": false}, {"data": ["findAllInvoice", 60, 0, 0.0, 5550.900000000001, 24, 11780, 9662.5, 11138.4, 11651.099999999999, 11780.0, 0.2011822812058866, 0.4226334152871709, 0.6468514606252744], "isController": false}, {"data": ["getChildChecklist", 29, 0, 0.0, 330.13793103448273, 272, 1393, 284.0, 341.0, 878.5, 1393.0, 0.0981344306341853, 0.23259010073160913, 0.3231536133774149], "isController": false}, {"data": ["getAllArea", 28, 0, 0.0, 111.71428571428572, 51, 242, 72.0, 186.0, 220.84999999999985, 242.0, 0.09977799396343137, 0.07699108323266447, 0.09383418768240664], "isController": false}, {"data": ["bankAccountInfoByIDChild", 29, 0, 0.0, 35.44827586206897, 30, 67, 32.0, 51.0, 62.5, 67.0, 0.10465082710239904, 0.15163374826784837, 0.2408808588675337], "isController": false}, {"data": ["findAllCreditDebitNotes", 28, 0, 0.0, 3608.6785714285716, 3302, 4432, 3519.5, 4275.1, 4410.4, 4432.0, 0.10473162521039836, 0.2033083680100991, 0.24464653076491488], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1298, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
